Styler
-------
This module allows a site's builder to customize the outcome of a view with wrap tag or custom CSS.

Features
--------------------------------------------------------------------------------
The primary features include:

* An administration interface to manage view block wrapper.

Standard usage scenario
--------------------------------------------------------------------------------
-- WORK IN PROGRESS --


Administration experience
--------------------------------------------------------------------------------
This module and its submodules gives the ability to add custom styles to outputs.


Migration / Upgrade from Drupal 7
--------------------------------------------------------------------------------
This module is not yet developped for Drupal 7

Credits / contact
--------------------------------------------------------------------------------
Currently maintained by Dickens A S

The best way to contact the authors is to submit an issue, be it a support
request, a feature request or a bug report, in the project issue queue:
  https://www.drupal.org/project/issues/styler
  
References
--------------------------------------------------------------------------------
1: https://www.drupal.org/project/metatag
 